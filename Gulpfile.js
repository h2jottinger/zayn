const DEBUG = process.env.NODE_ENV === 'debug';
const CI = process.env.CI === 'true';

const gulp = require('gulp');
const util = require('gulp-util');
const mocha = require('gulp-spawn-mocha');
const eslint = require('gulp-eslint');
const del=require('del');
const install=require('gulp-install');
const runSequence=require('run-sequence');
const awsLambda=require('node-aws-lambda');
const merge=require('merge-stream');
const zip=require('gulp-zip');

gulp.task('clean', function() {
  return del(['./dist', './dist.zip']);
});

gulp.task('js', function() {
  const index=gulp.src('index.js')
    .pipe(gulp.dest('dist/'));
  const lib=gulp.src('lib/*.js')
    .pipe(gulp.dest('dist/lib'));
  return merge(index, lib);
});

gulp.task('node-mods', function() {
  return gulp.src('./package.json')
    .pipe(gulp.dest('dist/'))
    .pipe(install({production: true}));
});

gulp.task('zip', function() {
  return gulp.src(['dist/**/*', '!dist/package.json'])
    .pipe(zip('dist.zip'))
    .pipe(gulp.dest('./'));
});

gulp.task('lint', () =>
  gulp.src(['index.js', 'lib/**/*.js', 'test/**/*.js'])
    .pipe(eslint())
    .pipe(eslint.format())
    // Brick on failure to be super strict
    .pipe(eslint.failOnError())
);

gulp.task('deploy', function(callback) {
  return runSequence(
    ['clean'],
    ['js', 'node-mods'],
    ['zip'],
    //['upload'],
    callback
  );
});

gulp.task('test', () =>
  gulp.src(['test/**/*.js'], {read: false})
    .pipe(mocha({
      debugBrk: DEBUG,
      R: CI ? 'spec' : 'nyan',
      timeout: 5000,
    }))
);

gulp.task('default', () =>
  gulp.watch(['lib/**', 'test/**'], ['lint', 'test'])
);