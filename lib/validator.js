const jsonLogic = require('json-logic-js');
const pluralize = require('pluralize');

const validateTemplate = function validateTemplate(template) {
  'use strict';

  if (!template) {
    throw new Error('missing template');
  }
  const result = jsonLogic.apply({
    if: [
      {
        missing: [
          'name',
          'description',
          'uuid',
          'ownerId',
          'constraints',
        ],
      },
    ],
  }, template);
  if (result.length) {
    throw new Error(`template missing ${pluralize('parameter', result.length)}: ${JSON.stringify(result)}`);
  }
};

module.exports.validateWithExplanation = function validateWithExplanation(template, data) {
  'use strict';

  const results = [];

  validateTemplate(template);

  if (!data) {
    results.push([false, 'invalid data set']);
  }
  else {
    const requiredRules = [
      {
        if: [
          {
            missing: ['templateId'],
          },
          false,
          true,
        ],
      },
      {
        '===': [
          {
            var: 'templateId',
          },
          template.name,
        ],
      },
    ];
    const allRules = requiredRules.concat(template.constraints);
    for (let i = 0; i < allRules.length; i += 1) {
      results.push([jsonLogic.apply(allRules[i], data), allRules[i]]);
    }
  }
  return results;
};

module.exports.validate = function validate(template, data) {
  'use strict';

  const validation = module.exports.validateWithExplanation(template, data);
  let result = true;
  for (let i = 0; i < validation.length; i += 1) {
    result = result && validation[i][0];
  }
  return result;
};

