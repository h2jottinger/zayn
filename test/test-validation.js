const chai = require('chai');
const validator = require('../lib/validator');

const expect = chai.expect;

describe('Validator', () => {
  'use strict';

  it('should throw exception for missing template', (done) => {
    const templates = [
      {
        uuid: 'aa-bb-cc',
        description: 'test rule',
        ownerId: 'joeo',
        constraints: [],
      },
      {
        name: 'Testrule',
        description: 'test rule',
        ownerId: 'joeo',
        constraints: [],
      },
      {
        name: 'Testrule',
        uuid: 'aa-bb-cc',
        ownerId: 'joeo',
        constraints: [],
      },
      {
        name: 'Testrule',
        uuid: 'aa-bb-cc',
        description: 'test rule',
        constraints: [],
      },
      {
        name: 'Testrule',
        uuid: 'aa-bb-cc',
        description: 'test rule',
        ownerId: 'joeo',
      },
    ];
    try {
      validator.validate();
      done('should have thrown exception for missing template data');
    }
    catch (e) {
      // continue, we expect this exception
    }
    for (let i = 0; i < templates.length; i += 1) {
      try {
        validator.validate(templates[i]);
        done('should have thrown exception for missing template data');
      }
      catch (e) {
        // continue, we expect this exception
      }
    }
    done();
  });
  it('should return false for missing data', (done) => {
    expect(validator.validate({
      name: 'Testrule',
      uuid: 'aa-bb-cc',
      description: 'test rule',
      ownerId: 'joeo',
      constraints: [],
    })).to.equal(false);
    done();
  });
  it('should return true for data with no constraints', (done) => {
    expect(validator.validate(
      {
        name: 'Testrule',
        uuid: 'aa-bb-cc-dd',
        description: 'test rule',
        ownerId: 'joeo',
        constraints: [],
      },
      {
        templateId: 'Testrule',
        a: 1,
      })).to.equal(true);
    done();
  });
  it('should return true for valid data with simple constraint', (done) => {
    expect(validator.validate(
      {
        name: 'Testrule',
        uuid: 'aa-bb-cc',
        description: 'test rule',
        ownerId: 'joeo',
        constraints: [
          {
            '<': [
              {
                var: 'a',
              },
              5,
            ],
          },
        ],
      },
      {
        templateId: 'Testrule',
        a: 1,
      })).to.equal(true);
    done();
  });
  it('should return false for invalid data with simple constraint', (done) => {
    expect(validator.validate(
      {
        name: 'Testrule',
        uuid: 'aa-bb-cc',
        description: 'test rule',
        ownerId: 'joeo',
        constraints: [
          {
            '<': [
              {
                var: 'a',
              },
              5,
            ],
          },
        ],
      },
      {
        a: 5,
      })).to.equal(false);
    done();
  });
  it('should return true for valid complex data with multiple constraints', (done) => {
    expect(validator.validate(
      {
        name: 'Testrule',
        uuid: 'aa-bb-cc',
        description: 'test rule',
        ownerId: 'joeo',
        constraints: [
          {
            '<': [
              {
                var: 'a',
              },
              5,
            ],
          },
          {
            '>': [
              {
                var: 'b',
              },
              1,
            ],
          },
        ],
      },
      {
        templateId: 'Testrule',
        a: 1,
        b: 2,
      })).to.equal(true);
    done();
  });
  it('should return false for invalid complex data with multiple constraints', (done) => {
    expect(validator.validate(
      {
        name: 'Testrule',
        uuid: 'aa-bb-cc',
        description: 'test rule',
        ownerId: 'joeo',
        constraints: [
          {
            '<': [
              {
                var: 'a',
              },
              5,
            ],
          },
          {
            '>': [
              {
                var: 'b',
              },
              1,
            ],
          },
        ],
      },
      {
        a: 1,
        b: 1,
      })).to.equal(false);
    done();
  });
  it('should identify male youths properly', (done) => {
    const data = [
      [
        {
          id: '12937',
          firstName: 'elroy',
          lastName: 'jetson',
          age: 6,
          gender: 'male',
          templateId: 'male youth',
        },
        true,
      ],
      [
        {
          id: '12937',
          firstName: 'sally',
          lastName: 'jetson',
          age: 14,
          gender: 'female',
          templateId: 'male youth',
        },
        false,
      ],
      [
        {
          id: '12937',
          firstName: 'george',
          lastName: 'jetson',
          age: 44,
          gender: 'male',
          templateId: 'male youth',
        },
        false,
      ],
      [
        {
          id: '12937',
          firstName: 'jane',
          lastName: 'jetson',
          age: 44,
          gender: 'female',
          templateId: 'male youth',
        },
        false,
      ],
      [
        {
          id: '12937',
          firstName: 'rosie',
          lastName: 'jetson',
          gender: 'male',
          templateId: 'male youth',
        },
        false,
      ],
    ];
    const template = {
      name: 'male youth',
      uuid: 'aa-bb-cc',
      description: 'test rule',
      ownerId: 'joeo',
      constraints: [
        {
          if: [
            {
              missing: ['age', 'firstName', 'lastName', 'gender'],
            },
            false,
            true,
          ],
        },
        {
          '===': [
            {
              var: 'gender',
            },
            'male',
          ],
        },
        {
          '<': [
            {
              var: 'age',
            },
            19,
          ],
        },
      ],
    };
    for (let i = 0; i < data.length; i += 1) {
      expect(validator.validate(template, data[i][0])).to.equal(data[i][1]);
    }
    done();
  });
});
